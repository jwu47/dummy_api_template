#!/usr/bin/env groovy

podTemplate(
    label: 'dnc-sdk-2',
    containers: [
        containerTemplate(
            name: 'jnlp',
            image: 'ngsnonprod.azurecr.io/aci_k8s/jenkins-slave-dnc-sdk:2.0',
            ttyEnabled: true,
            command: 'jenkins-slave',
            privileged: false,
            alwaysPullImage: true,
            workingDir: '/home/jenkins',
            args: '${computer.jnlpmac} ${computer.name}')
        ],
    volumes: [
        secretVolume(secretName: 'registrylogin', mountPath: '/registrylogin'),
        hostPathVolume(mountPath: '/usr/bin/docker', hostPath: '/usr/bin/docker'),
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
        hostPathVolume(mountPath: '/root/.kube', hostPath: '/root/.kube')
    ]
)

{
  node('dnc-sdk-2') {
    REPO_PROJ = 'mic_dnc'
    MJR_MIN_VER = '1.0'
    SECRET_VOL = 'registrylogin'
    SLACK_CHANNEL = '#microservice-ci'

    try{

      stage('checkout') {
        //   slackSend channel: "${SLACK_CHANNEL}", color: 'good', message: "started ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
          bitbucketStatusNotify( buildState: 'INPROGRESS' )
          checkout scm
      }

      stage('build & publish') {
        sh """./build.sh
          set +x
          REPO=`cat /${SECRET_VOL}/url`
          USERNAME=`cat /${SECRET_VOL}/username`
          PASSWORD=`cat /${SECRET_VOL}/password`
          docker login \$REPO -u \$USERNAME -p \$PASSWORD
          for df in Dockerfile* ; do 

            REPO_NAME=`echo "\$df" | sed -r 's/Dockerfile_//g'`
            BUILD_VERSION=${MJR_MIN_VER}.${env.BUILD_NUMBER}
            sed -i "s#_insert-version_#\$BUILD_VERSION#" \$df

            TAG="\$REPO/${REPO_PROJ}/\$REPO_NAME:\$BUILD_VERSION"
            TAG_VNEXT="\$REPO/${REPO_PROJ}/\$REPO_NAME:v-next"
            
            docker build --no-cache -t \$TAG -f \$df .
            echo "------- \$REPO_NAME BUILD FINISHED -------"
            
            docker push \$TAG
            docker tag \$TAG \$TAG_VNEXT
            docker push \$TAG_VNEXT
            echo "------- \$REPO_NAME PUBLISHED -------"
          done
        """
        bitbucketStatusNotify( buildState: 'SUCCESSFUL' )
        // slackSend channel: "${SLACK_CHANNEL}", color: 'good', message: "completed ${env.JOB_NAME} ${env.BUILD_NUMBER}"
      }
    }
    catch (any) {
        // slackSend channel: "${SLACK_CHANNEL}", color: 'danger', message: "failed ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
        bitbucketStatusNotify( buildState: 'FAILURE', buildDescription: 'build failed.' )
        currentBuild.result = 'FAILURE'
        throw any //rethrow exception to prevent the build from proceeding
    }
    finally {
        step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: 'jwester@newgistics.com', sendToIndividuals: true])
    }
  }
}
