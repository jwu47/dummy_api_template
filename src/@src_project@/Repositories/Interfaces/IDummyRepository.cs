﻿using Newgistics.Lib.ServiceHost.Dto;
using System.Threading.Tasks;

namespace @proj_namespace@.Repositories.Interfaces
{
    public interface IDummyRepository
    {
        Task<BooleanResult> GetDummy(string id);
    }
}
